package home.tannhn.sp.utils;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import home.tannhn.sp.entity.ContactEntity;

@Mapper(componentModel = "spring")
public interface ContactMapper {
	void updateCustomerFromDto(ContactEntity dto, @MappingTarget ContactEntity entity);

}
