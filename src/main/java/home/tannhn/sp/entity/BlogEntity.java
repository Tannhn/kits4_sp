package home.tannhn.sp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.processing.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.engine.internal.CascadePoint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tbl_blog")
public class BlogEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
//	@Column(name="title_test", nullable = false, unique = true, updatable = false, 
//			length = 300 , insertable = true  )
	private String title;
	
	

	
//	@Transient
//	private int totalComments;
	
	@OneToOne(cascade = CascadeType.PERSIST, orphanRemoval = false)
	@JoinColumn(name="author_id")
	private UserEntity author;	

	
//	@OneToMany
//	@JoinColumn(name="blog_id")
//	private List<CommentEntity> comments = new ArrayList<CommentEntity>();
	
//	@ManyToMany
//	@JoinTable(name="tbl_blog_tag",
//	joinColumns = {@JoinColumn (name="blog_id")},
//	inverseJoinColumns = {@JoinColumn(name="tag_id")}
//			)
//	private List<TagEntity> tags = new ArrayList<TagEntity>();
	
	
}

