package home.tannhn.sp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="tbl_comment")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentEntity {

//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="comment_sequence")
//	@SequenceGenerator(name="comment_sequence", allocationSize = 5, sequenceName = "comment_seq", initialValue = 5 )
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String content;
	//@JoinColumn(name="blog_id")
	
	//@ManyToOne
	//private BlogEntity blog;

}
