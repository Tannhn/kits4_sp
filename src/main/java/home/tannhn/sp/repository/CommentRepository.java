package home.tannhn.sp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import home.tannhn.sp.entity.CommentEntity;

public interface CommentRepository extends JpaRepository<CommentEntity, Integer>{

}
