package home.tannhn.sp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import home.tannhn.sp.entity.BlogEntity;

public interface BlogRepository extends JpaRepository<BlogEntity, Integer> {
	
	BlogEntity findByAuthor_id(Integer id);
	
	BlogEntity findByTitle(String title);
	
	BlogEntity findByTitleIsNot(String title);
	
	BlogEntity findByTitleIsNull();
	
	BlogEntity findByTitleIsNotNull();
	
	List<BlogEntity> findByActiveTrue();
	
	List<BlogEntity> findByAuthorUsername();
}
