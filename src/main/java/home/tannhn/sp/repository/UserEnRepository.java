package home.tannhn.sp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import home.tannhn.sp.entity.UserEntity;

public interface UserEnRepository extends JpaRepository<UserEntity, Integer> {

}
