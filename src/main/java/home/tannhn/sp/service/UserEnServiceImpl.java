package home.tannhn.sp.service;

import java.util.Enumeration;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.catalina.startup.UserConfig;
import org.apache.catalina.startup.UserDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import home.tannhn.sp.entity.UserEntity;
import home.tannhn.sp.repository.UserEnRepository;

@Service
@Transactional
public class UserEnServiceImpl implements UserEnService, UserDatabase {

	@Autowired
	UserEnRepository userRepo;
	
	@Override
	public List<UserEntity> list() {
		return userRepo.findAll();
	}

	@Override
	public UserEntity save(UserEntity entity) {
		// TODO Auto-generated method stub
		return userRepo.save(entity);
	}

	@Override
	public UserEntity update(UserEntity entity) {
		// TODO Auto-generated method stub
		return userRepo.save(entity);
	}

	@Override
	public UserEntity getOne(Integer key) {
		// TODO Auto-generated method stub
		return userRepo.getById(key);
	}

	@Override
	public void delete(Integer key) {
		// TODO Auto-generated method stub
		userRepo.deleteById(key);
	}

	@Override
	public UserConfig getUserConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setUserConfig(UserConfig userConfig) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getHome(String user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration<String> getUsers() {
		// TODO Auto-generated method stub
		return null;
	}

}
