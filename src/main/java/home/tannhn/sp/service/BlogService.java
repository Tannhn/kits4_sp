package home.tannhn.sp.service;

import home.tannhn.sp.entity.BlogEntity;

public interface BlogService extends BaseService<BlogEntity, Integer>{
	BlogEntity findUserByAuthor_id(Integer id);
	
	BlogEntity findUserByTitle(Integer id);
}
