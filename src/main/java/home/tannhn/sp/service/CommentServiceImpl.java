package home.tannhn.sp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import home.tannhn.sp.entity.BlogEntity;
import home.tannhn.sp.entity.CommentEntity;
import home.tannhn.sp.repository.CommentRepository;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {
	
	@Autowired
	CommentRepository commentRepo;
	
	@Override
	public List<CommentEntity> list() {
		return commentRepo.findAll();
	}

	@Override
	public CommentEntity save(CommentEntity entity) {
		return commentRepo.save(entity);
	}

	@Override
	public CommentEntity update(CommentEntity entity) {
		return commentRepo.save(entity);
	}

	@Override
	public CommentEntity getOne(Integer key) {
		return commentRepo.getById(key);
	}

	@Override
	public void delete(Integer key) {
		commentRepo.deleteById(key);

	}

}
