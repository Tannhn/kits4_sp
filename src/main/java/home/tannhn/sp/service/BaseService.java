package home.tannhn.sp.service;

import java.util.List;

import home.tannhn.sp.entity.BlogEntity;

public interface BaseService<T, K> {
	List<T> list();
	T save (T entity);
	T update(T entity);
	T getOne(K key);
	void delete(K key);
}
