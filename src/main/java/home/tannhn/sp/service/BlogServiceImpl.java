package home.tannhn.sp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import home.tannhn.sp.entity.BlogEntity;
import home.tannhn.sp.repository.BlogRepository;

@Service
@Transactional
public class BlogServiceImpl implements BlogService {
	
	@Autowired
	BlogRepository blogRepo;
	
	@Override
	public List<BlogEntity> list() {
		return blogRepo.findAll();
	}

	@Override
	public BlogEntity save(BlogEntity blog) {
		return blogRepo.save(blog);
		
	}

	@Override
	public BlogEntity update(BlogEntity blog) {
		return blogRepo.save(blog);
	}

	@Override
	public BlogEntity getOne(Integer id) {
		return blogRepo.getOne(id);
	}

	@Override
	public void delete(Integer id) {
		blogRepo.deleteById(id);
	}

	@Override
	public BlogEntity findUserByAuthor_id(Integer id) {
		return blogRepo.findByAuthor_id(id);
	}
	
}
