package home.tannhn.sp.controller;

import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import home.tannhn.sp.entity.BlogEntity;
import home.tannhn.sp.entity.CommentEntity;
import home.tannhn.sp.entity.UserEntity;
import home.tannhn.sp.service.BlogService;
import home.tannhn.sp.service.CommentService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/blogs")
public class BlogController {
	
	@Autowired
	BlogService blogService;
	@Autowired
	CommentService cmtService;
	
	@GetMapping("")
	public ResponseEntity<?> listBlog() {
		log.info("listBlog");
		
		List<BlogEntity> list = blogService.list(); 
		
		return new ResponseEntity<List<BlogEntity>>(list, HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<?> createBlog(@RequestBody BlogEntity blog) {
		log.info("createBlog");
		
		BlogEntity blogEn = blogService.save(blog);
		
		return new ResponseEntity<BlogEntity>(blogEn, HttpStatus.OK);
	}
	
	@PutMapping("/{blogId}")
	public ResponseEntity<?> updateBlog(@PathVariable Integer blogId, @RequestBody BlogEntity blog) {
		
		log.info("updateBlog {}", blogId);
		
		BlogEntity blogEn = null;
		if(blogService.getOne(blogId) != null) {
			blogEn = blogService.update(blog);
		} else {
			return new ResponseEntity<String>("error", HttpStatus.OK);
		}

		return new ResponseEntity<BlogEntity>(blogEn, HttpStatus.OK);
	}
	
	@DeleteMapping("/{blogId}")
	public ResponseEntity<?> deleteBlog(@PathVariable Integer blogId) {
		
		log.info("delete Blog {}", blogId);
		
		blogService.delete(blogId);
		
		return new ResponseEntity<String>("success", HttpStatus.OK);
	}
	
	@GetMapping("/author/{authorId}")
	public ResponseEntity<?> findBlogByAuthor(@PathVariable Integer authorId) {
		
		log.info("find Blog by Author {}", authorId);
		
		BlogEntity blog = blogService.findUserByAuthor_id(authorId);
		
		return new ResponseEntity<BlogEntity>(blog, HttpStatus.OK);
	}
	
	@GetMapping("/test")
	public ResponseEntity<?> test() {
		
//		BlogEntity blog = blogService.getOne(1);
//		
//		blog.getComments().remove(0);
//		System.out.println(blog);
//		
//		blogService.save(blog);
		
		//////////////////
		/* save blog with cascadetype.persist 
		 * without cascadetype.persist  => must save comment before save blog
		 * with cascadetype.persist => save in parent also apply on chidlren
		 * */
		
//		BlogEntity blog = new BlogEntity();
//		CommentEntity comment1 = new CommentEntity();
//		
//		comment1.setContent("comment so 1");
//		//CommentEntity comment = cmtService.save(comment1);
//		blog.setTitle("Test CascadeType");
//		blog.setComments(Arrays.asList(comment1));
//		blogService.save(blog);

		///////////////////////
		/* update blog with cascadety.persist 
		 * => update not affect, update parent also update children
		 * */
//		
//		BlogEntity blog = blogService.getOne(7);
//		blog.getComments().get(0).setContent("aaaa");
//		blog.getComments().get(1).setContent("bbbb");
//		blogService.save(blog);
		
		
		///////////////////////
		/* delete uni-directional
		*  (1) delete without cascadetype => delete parent, not delete children, just set null where children reference parent
		*  (2) delete with cascadetype.persist => like (1)
		*  (3) delete with cascadetype.remove => check if blog exist, check if comments exist, set null blog filed in comment, delete comment, delete blog
		* */
		//blogService.delete(7);
		
		
		///////////////////////
		/* delete uni-directional
		*  (1) delete blog without cascadetype => delete parent, not delete children, just set null where children reference parent
		*  (2) delete blog with cascadetype.persist => like (1)
		*  (3) delete blog with cascadetype.remove => check if blog exist, check if comments exist, set null blog filed in comment, delete comment, delete blog
		*  (4) delete blog with orphanRemoval = true => delete parent and all children
		*  ===delete children =====
		*  (4) delete relationship of comment in blog: 
		*  		(a) if have cascade = CascadeType.ALL or PERSIST=> delete all comments not in  relationship 
		*  		(b) if not cascade  => just set null in blog_id field
		* */
		
//		BlogEntity blog = blogService.getOne(7);
//		blog.getComments().remove(0);
//		blogService.save(blog);
//		blogService.delete(7);
		
		///////////////////////
		/* @OneToOne - FetchType
		*  FetchType = eager => left outer join 
		*  FetchType = lazy => select blog , select user
		* */
		
//		BlogEntity blog = blogService.getOne(1);
//		System.out.println("chua select blog");
//		blog.getAuthor();
//		System.out.println("select blog sau do select user");
		
		///////////////////////
		/* @OneToOne - CascadeType
		*  Persist =>  action "Create" on parent also apply on children, create child first, after parent
		*  FetchType = lazy => select blog , select user
		* */
		
//		BlogEntity blog = new BlogEntity();
//		blog.setTitle("tannn");
//		UserEntity user = new UserEntity();
//		blog.setAuthor(user);
//		blogService.save(blog);
//		blogService.delete(7);
		///////////////////////
		/* @OneToOne - orphanRemoval
		*  yes =>  remove "orphan" associate 
		*  no = lazy => set null in field
		* */
		BlogEntity blog = blogService.getOne(7);
		blog.setAuthor(null);
		blogService.save(blog);
		return new ResponseEntity<String>("s1ccess", HttpStatus.OK);
	}
}
