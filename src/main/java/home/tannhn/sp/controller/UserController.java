package home.tannhn.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import home.tannhn.sp.entity.UserEntity;
import home.tannhn.sp.service.UserEnService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/users")
public class UserController {
	/*Mapping @OneToOne Annotation
	 *  1. only put @JoinColumn on table with FK
	 * */
	@Autowired
	UserEnService userService;
	
	@GetMapping("")
	public ResponseEntity<?> listUser() {
		 log.info("get all user");
		 
		 List<UserEntity> list = userService.list();
		 
		 return new ResponseEntity<List<UserEntity>>(list, HttpStatus.OK);
	}
}
