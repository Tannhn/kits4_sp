package home.tannhn.sp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Kits4SpingbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Kits4SpingbootApplication.class, args);
	}

}
